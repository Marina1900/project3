public abstract class Figure {
    public double fX;
    public double fY;
    public abstract void square();
    public Figure(double x, double y){
        fX = x;
        fY = y;
    }
    public void getQuadrant() {
        if (fX > 0 && fY > 0) {
            System.out.println("Центр фигуры лежит в первой четверти");
        }
        if (fX < 0 && fY > 0) {
            System.out.println("Центр фигуры лежит во второй четверти");
        }
        if (fX < 0 && fY < 0) {
            System.out.println("Центр фигуры лежит в третьей четверти");
        }
        if (fX > 0 && fY < 0) {
            System.out.println("Центр фигуры лежит в четвертой четверти");
        }
        if (fX == 0 || fY == 0) {
            System.out.println("Центр фигуры лежит на координатной оси");
        }
    }
}